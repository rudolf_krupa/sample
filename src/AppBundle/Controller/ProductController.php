<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends Controller
{

    // requirements={"id": "\d+"}

    /**
     * @Route("/product-detail/{$id}", name="product-detail")
     */
    public function detailAction($id)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // cache
        $cache = $this->container->get('app.cache');

        $product = $cache->getItem('test');

        if(!$product->isHit()) {
            $product->set(['id' => 999, 'sample' => 'asdfasdaf']);
            $cache->save($product);
        }

        $productOutput = $product->get();

        var_dump($productOutput);

        $cache->deleteItem('test');

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        return new JsonResponse(['id' => $id, 'name' => 'price']);
    }
}